﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="LandingPage_Default" %>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Oxi Clean</title>
    <link rel="preconnect" href="https://maxcdn.bootstrapcdn.com">
    <link rel="preconnect" href="https://cdnjs.cloudflare.com">
    <link rel="preconnect" href="https://code.jquery.com/">
    <link rel="preconnect" href="https://cdn.jsdelivr.net/">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
      integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/oxiclean/css/site.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/oxiclean/img/favicon/favicon.png" />
    <link rel="apple-touch-icon" href="/oxiclean/img/favicon/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/oxiclean/img/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/oxiclean/img/favicon/apple-touch-icon-114x114.png" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162929067-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());

      gtag('config', 'UA-162929067-1');
    </script>
  </head>

  <body>
    <header id="frmtHead">
      <div class="container">
        <div class="row">
          <div class="col-4">

          </div>
          <div class="col-4 text-center">
            <a href="/">
              <img src="/oxiclean/img/blank.png" data-src="/oxiclean/img/farmasi-gold.png" class="lazyload img-fluid"
                alt="Oxi Clean" width="170" height="40">
            </a>
          </div>
          <div class="col-4 text-right">
            <button id="menu-btn" onclick="menuOpen()">
              <svg width="24" height="24" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 24H42" stroke="#AD8E2F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M6 12H42" stroke="#AD8E2F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M6 36H42" stroke="#AD8E2F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </header>
    <div id="menu">
      <button class="close-menu" onclick="menuClose()">
        <svg style="width: 24px; height: 24px" viewBox="0 0 24 24">
          <path fill="#fff"
            d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z">
          </path>
        </svg>
      </button>
      <ul id="menuList">
        <li><a href="#section1" class="scroll">Yeni Oxi Clean</a></li>
        <li><a href="#section2" class="scroll">İçerik</a></li>
        <li><a href="#video" class="scroll">Video</a></li>
        <li><a href="#section3" class="scroll">Tüm Renkler</a></li>
        <li><a href="#section4" class="scroll">Zorlu Lekeler</a></li>
        <li><a href="#section4-5" class="scroll">Makine Dostu</a></li>
        <li><a href="#section5" class="scroll">Kullanım</a></li>
      </ul>
    </div>
    <section id="section1">
      <a href="https://www.farmasi.com.tr/mr-wipes-performans-oxi-clean-leke-cikarici-1000-g/11595">
        <img src="/oxiclean/img/blank.png" data-src="/oxiclean/img/section1-dsk.jpg" alt="Oxi Clean" class="lazyload">
      </a>
    </section>
    <section id="section2">
      <picture>
        <source media="(max-width: 1023px)" srcset="/oxiclean/img/blank.png" data-srcset="/oxiclean/img/section2-mbl.jpg" class="lazyload">
        <source media="(min-width: 1024px)" srcset="/oxiclean/img/blank.png" data-srcset="/oxiclean/img/section2-dsk.jpg" class="lazyload">
        <img srcset="/oxiclean/img/blank.png" data-src="/oxiclean/img/section2-dsk.jpg" alt="Oxi Clean" class="lazyload">
      </picture>
    </section>
    <div id="video" class="jarallax">
      <div class="vid-container">
        <iframe width="560" height="315" data-src="https://www.youtube.com/embed/GRLqWOROzDA?rel=0" frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen=""></iframe>
      </div>
    </div>
    <section id="section3">
      <img src="/oxiclean/img/blank.png" data-src="/oxiclean/img/section4-dsk.jpg" alt="Oxi Clean" class="lazyload">
    </section>
    <section id="section4">
      <picture>
        <source media="(max-width: 1023px)" srcset="/oxiclean/img/blank.png" data-srcset="/oxiclean/img/section3-mbl.jpg" class="lazyload">
        <source media="(min-width: 1024px)" srcset="/oxiclean/img/blank.png" data-srcset="/oxiclean/img/section3-dsk.jpg" class="lazyload">
        <img srcset="/oxiclean/img/blank.png" data-src="/oxiclean/img/section3-dsk.jpg" alt="Oxi Clean" class="lazyload">
      </picture>
    </section>
    <section id="section4-5">
      <img src="/oxiclean/img/blank.png" data-src="/oxiclean/img/section4-5.jpg" alt="Oxi Clean" class="lazyload">
    </section>
    <section id="section5">
      <picture>
        <source media="(max-width: 1023px)" srcset="/oxiclean/img/blank.png" data-srcset="/oxiclean/img/section5-mbl.jpg" class="lazyload">
        <source media="(min-width: 1024px)" srcset="/oxiclean/img/blank.png" data-srcset="/oxiclean/img/section5-dsk.jpg" class="lazyload">
        <img srcset="/oxiclean/img/blank.png" data-src="/oxiclean/img/section5-dsk.jpg" alt="Oxi Clean" class="lazyload">
      </picture>
    </section>
    <footer id="frmFooter">
      <ul>
        <li>
          <a href="https://www.facebook.com/FarmasiTurkeyOfficial/" target="_blank" rel="noreferrer">
            <img src=" img/blank.png" data-src="/oxiclean/img/facebook.png" alt="Oxi Clean" class="lazyload">
          </a>
        </li>
        <li>
          <a href="https://www.instagram.com/farmasiofficial/" target="_blank" rel="noreferrer">
            <img src=" img/blank.png" data-src="/oxiclean/img/instagram.png" alt="Oxi Clean" class="lazyload instagram">
          </a>
        </li>
        <li>
          <a href="https://www.youtube.com/channel/UCBT7TowgH5y9zM6lPtZadMg?view_as=subscriber" target="_blank"
            rel="noreferrer">
            <img src=" img/blank.png" data-src="/oxiclean/img/youtube.png" alt="Oxi Clean" class="lazyload">
          </a>
        </li>
      </ul>
      <p>© 2021 Farmasi</p>
    </footer>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
      integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/1.12.7/jarallax.min.js"
      integrity="sha512-JpiSiPEgPmeTAnXyH375a9UxAsjoiyO+lpsayQd7Pcl2+tYf4y1beIF25yDMvmAV7dbZUPqh084iT3D6IQOoHg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="/oxiclean/js/core.min.js"></script>
  </body>

  </html>