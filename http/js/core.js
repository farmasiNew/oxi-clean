var menuOpen = function () {
  $("#menu").addClass("active")
},
  menuClose = function () {
    $("#menu").removeClass("active")
  };
$("a.scroll").bind("click", function (e) {
  menuClose();
});
$("#lang-wrapper .active").bind("click", function (e) {
  e.preventDefault()
});
$(window).on('load', function () {
  //lazyload();
  $('.lazyload').each(function(){
    $(this).attr('src', $(this).attr('data-src'))
    $(this).attr('srcset', $(this).attr('data-srcset'))
  });
  $('#video iframe').attr('src', $('#video iframe').attr('data-src'));
  
  $('.jarallax').jarallax();

});
$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};